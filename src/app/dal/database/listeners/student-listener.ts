import { Student } from './../models/student.model';
import { BaseDataListener } from './../base-data-listener';

export interface StudentListener extends BaseDataListener<Student> {
    // Extends whit more methods when need.

}
