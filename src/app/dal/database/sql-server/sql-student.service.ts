import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

import { StudentListener } from './../listeners/student-listener';
import { Student } from './../models/student.model';

@Injectable({
    providedIn: 'root'
})
export class SqlStudentService implements StudentListener {

    protected readonly data: BehaviorSubject<Student[]> = new BehaviorSubject<Student[]>([
        { id: '1', name: 'Student from SqlServer', age: 28 },
        { id: '2', name: 'Student from SqlServer 2', age: 28 }
    ]);

    getItem(id: string): Observable<Student> {
        console.log('Called SqlServer database');
        return this.data.pipe(
            delay(500),
            map((students: Student[]) => students.filter(student => student.id === id).pop())
        );
    }

    getItems(): Observable<Student[]> {
        return this.data.pipe(delay(500));
    }

    saveItem(item: Student): Observable<any> {
        this.data.next([...this.data.value, item]);
        return of(EMPTY).pipe(delay(500));
    }

    saveItems(items: Student[]): Observable<any> {
        this.data.next([...this.data.value, ...items]);
        return of(EMPTY).pipe(delay(500));
    }

    deleteItem(id: string): Observable<any> {
        this.data.next(this.data.value.filter(student => student.id !== id));
        return of(EMPTY).pipe(delay(500));
    }

    deleteItems(ids: string[]): Observable<any> {
        this.data.next(this.data.value.filter(student => !ids.includes(student.id)));
        return of(EMPTY).pipe(delay(500));
    }
}
