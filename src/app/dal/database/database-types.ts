import { OracleDatabase } from './oracle/oracle.database';
import { SqlServerDatabase } from './sql-server/sql-server.database';

export class DatabaseTypes {
    static Oracle = OracleDatabase;
    static SqlServer = SqlServerDatabase;
}
