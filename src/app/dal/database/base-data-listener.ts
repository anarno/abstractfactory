import { Observable } from 'rxjs';

export interface BaseDataListener<T> {
    getItem(id?: string): Observable<T>;

    getItems(): Observable<T[]>;

    saveItem(item: T): Observable<any>;

    saveItems(items: T[]): Observable<any>;

    deleteItem(id: string): Observable<any>;

    deleteItems(ids: string[]): Observable<any>;
}
