import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { StudentComponent } from './components/student/student.component';

@NgModule({
    declarations: [StudentComponent],
    imports: [CommonModule, StudentRoutingModule, FormsModule, ReactiveFormsModule]
})
export class StudentModule {}
