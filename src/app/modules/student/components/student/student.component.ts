import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { StudentPresenter } from 'src/app/dal/database/presenters/student.presenter';

import { Student } from './../../../../dal/database/models/student.model';

@Component({
    selector: 'app-student',
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
    student$: Observable<Student>;
    students$: Observable<Student[]>;
    errorMessages = {
        required: 'This field is required!',
        minlength: 'The minimum length is 6 chars!',
        min: 'Minimum value is 1!'
    };
    nameCtrl: FormControl = new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)]));
    ageCtrl: FormControl = new FormControl('', Validators.compose([Validators.required, Validators.min(1)]));
    form: FormGroup = new FormGroup({
        name: this.nameCtrl,
        age: this.ageCtrl
    });

    constructor(private studentService: StudentPresenter) {}

    ngOnInit() {
        this.student$ = this.studentService.getItem('1');
        this.students$ = this.studentService.getItems();
    }

    saveStudent(item: Student): void {
        if (this.form.valid) {
            const subs = this.studentService.saveItem({ id: Date.now().toString(), ...item }).subscribe(() => {
                console.log('Student saved!');
                subs.unsubscribe();
            });
        }
    }

    deleteStudent(id: string): void {
        const subs = this.studentService.deleteItem(id).subscribe(() => {
            console.log('Item removed!');
            subs.unsubscribe();
        });
    }

    getErrors(errors: ValidationErrors): string[] {
        return errors ? Object.keys(errors) : [];
    }
}
