import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { StudentListener } from './../listeners/student-listener';
import { Student } from './../models/student.model';

@Injectable({
    providedIn: 'root'
})
export class StudentPresenter implements StudentListener {
    private studentDataListener: StudentListener = this.injector.get(environment.database.student);

    constructor(private injector: Injector) {}

    getItem(id: string): Observable<Student> {
        return this.studentDataListener.getItem(id);
    }

    getItems(): Observable<Student[]> {
        return this.studentDataListener.getItems();
    }

    saveItem(item: Student): Observable<void> {
        return this.studentDataListener.saveItem(item);
    }

    saveItems(items: Student[]): Observable<void> {
        return this.studentDataListener.saveItems(items);
    }

    deleteItem(id: string): Observable<any> {
        return this.studentDataListener.deleteItem(id);
    }

    deleteItems(ids: string[]): Observable<any> {
        return this.studentDataListener.deleteItems(ids);
    }
}
